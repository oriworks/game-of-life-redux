import { Provider } from 'react-redux';
import BoardType from '../components/board-type';
import Board from '../components/board';
import configureStore from './store';

const store  = configureStore({})

function App() {
  return (
    <Provider store={store}>
      <BoardType />
      <Board />
    </Provider>
  );
}

export default App;
