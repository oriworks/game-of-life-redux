import { applyMiddleware, createStore } from "redux"
import { composeWithDevTools } from "redux-devtools-extension"
import thunk from "redux-thunk"

import { reducer } from "../store"
import * as services from "../services"

const configureStore = (preloadedState) => {
	const thunkMiddleware = thunk.withExtraArgument(services)

	const middlewares = [thunkMiddleware]
	const middlewareEnhancer = applyMiddleware(...middlewares)

	const enhancers = [middlewareEnhancer]
	const composedEnhancers = composeWithDevTools(...enhancers)

	const store = createStore(reducer, preloadedState, composedEnhancers)

	return store
}

export default configureStore