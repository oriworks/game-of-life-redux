import React, { useCallback, useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { changeBoardType, loadBoardTypes } from "../../store/creation-menu/actions";
import { selectors as creationMenuSelectors } from "../../store/creation-menu";

const BoardType = () => {
    // State
    const boardType = useSelector(creationMenuSelectors.getBoardType)
    const boardTypes = useSelector(creationMenuSelectors.getBoardTypes)
    
    // Dispatch
    const dispatch = useDispatch();

    // Handlers
    const handleChange = useCallback((event) => {
        dispatch(changeBoardType(event.target.value))
    }, [dispatch])

    // Effects
    useEffect(() => {
        dispatch(loadBoardTypes())
    }, [dispatch])

    return (<select value={boardType} onChange={handleChange}>
        <option value="">Please select type.</option>
        { boardTypes.map(boardType => (<option key={`option-${boardType}`} value={boardType}>{ boardType.toUpperCase() }</option>)) }
    </select>)
}

export default BoardType