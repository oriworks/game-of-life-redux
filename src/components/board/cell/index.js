import React, { useCallback } from "react";
import { useDispatch } from "react-redux";
import PropTypes from 'prop-types';

import { actions as boardActions } from "../../../store/board";

import styles from './styles.module.css'

const propTypes = {
    active: PropTypes.bool,
    x: PropTypes.number,
    y: PropTypes.number,
}

const Cell = ({ active, x, y }) => {
    // Dispatch
    const dispatch = useDispatch()

    // Handler
    const handleClick = useCallback(() => {
        dispatch(boardActions.uploadBoard(x, y))
    }, [dispatch, x, y])

    return <div className={styles.cell} onClick={handleClick}>{ active ? 1 : 0}</div>
}

Cell.propTypes = propTypes;

export default Cell