import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";

import { selectors as boardSelectors, actions as boardActions } from "../../store/board";

import styles from './styles.module.css'
import Cell from "./cell";

const Board = () => {
    // State
    const board = useSelector(boardSelectors.getBoard)

    // Dispatch
    const dispatch = useDispatch()

    // Effects
    useEffect(() => {
        dispatch(boardActions.createBoard())
    }, [dispatch])

    return (
        <div className={styles.wrapper}>
            <div className={styles.board} >
                { board ? board.map(
                    (line, y) => (
                        <div key={`line-${y}`} className={styles.line}>
                            {line.map((cell, x) => 
                                (<Cell key={`cell-${x}-${y}`} active={cell} x={x} y={y}/>))}
                        </div>)
                    ) : 'Não existe board' }
            </div>
        </div>
    )
}

export default Board