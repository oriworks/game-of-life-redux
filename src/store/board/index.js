import * as actionTypes from './action-types';
import * as actions from './actions';
import * as selectors from './selectors';
import reducer from './reducer';

export default reducer;

export { reducer, actionTypes, actions, selectors };
