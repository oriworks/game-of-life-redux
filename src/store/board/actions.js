import * as actionTypes from './action-types';

import { selectors as creationMenuSelectors } from "../creation-menu";
import * as boardSelectors from "./selectors";

export const createBoard = () => async (dispatch, getState, { board }) => {
    try {
        const state = getState()
        const size = creationMenuSelectors.getBoardSize(state)
        const type = creationMenuSelectors.getBoardType(state)
        const { data } = await board.createBoard(size, type);

        dispatch({
            type: actionTypes.CREATE_BOARD,
            payload: data
        });
    } catch (error) {
        throw error
    }
}

export const uploadBoard = (x, y) => async (dispatch, getState, { board }) => {
    try {
        const boardState = boardSelectors.getBoard(getState());
        
        const { data } = await board.uploadBoard(
            boardState.map(
                (line, iy) => line.map(
                    (active, ix) => x === ix && y === iy ? !active : active
                )
            )
        );

        dispatch({
            type: actionTypes.UPLOAD_BOARD,
            payload: data
        });
    } catch (error) {
        throw error
    }
}