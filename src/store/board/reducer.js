import { combineReducers } from 'redux';
import * as actionTypes from './action-types';

const initialState = {
    data: null,
}

const data = (state = initialState.data, action) => {
    switch (action.type) {
        case actionTypes.CREATE_BOARD:
        case actionTypes.UPLOAD_BOARD:
            return action.payload
        default:
            return state;
    }
}

export default combineReducers({
    data,
})