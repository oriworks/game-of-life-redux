import { combineReducers } from "redux";

import board from './board'
import creationMenu from './creation-menu'

export const reducer = combineReducers({
    board,
    creationMenu
})