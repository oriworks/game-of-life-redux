
export const getBoardSize = state => state.creationMenu.boardSize
export const getBoardTypes = state => state.creationMenu.boardTypeList

export const getBoardType = state => state.creationMenu.boardTypeSelected