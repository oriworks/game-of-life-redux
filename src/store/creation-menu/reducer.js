import { combineReducers } from 'redux';
import * as actionTypes from './action-types';

const initialState = {
    boardSize: 6,
    boardTypeList: [],
    boardTypeSelected: 'random'
}

const boardSize = (state = initialState.boardSize, action) => {
    switch (action.type) {
        case actionTypes.CHANGE_BOARD_SIZE:
            return action.payload
        default:
            return state;
    }
}

const boardTypeList = (state = initialState.boardTypeList, action) => {
    switch (action.type) {
        case actionTypes.LOAD_BOARD_TYPES:
            return action.payload
        default:
            return state;
    }
}

const boardTypeSelected = (state = initialState.boardTypeSelected, action) => {
    switch (action.type) {
        case actionTypes.CHANGE_SELECTED_BOARD_TYPE:
            return action.payload;
        default:
            return state;
    }
}

export default combineReducers({
    boardSize,
    boardTypeList,
    boardTypeSelected
})