export const CHANGE_BOARD_SIZE = 'creation-menu/change-board-size'
export const LOAD_BOARD_TYPES = 'creation-menu/load-board-types'
export const CHANGE_SELECTED_BOARD_TYPE = 'creation-menu/change-selected-board-type'