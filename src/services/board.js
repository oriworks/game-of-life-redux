import axios from "axios";

export const getBoardTypes = () => axios.get('http://localhost:3001/boards')

export const createBoard = (size, type) => axios.post('http://localhost:3001/board', { size, type })
export const uploadBoard = (board) => axios.post('http://localhost:3001/board/import', { board })